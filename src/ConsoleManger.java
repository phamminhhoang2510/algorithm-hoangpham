import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ConsoleManger {
    Scanner scan = new Scanner(System.in);

    ProductManager productManager = new ProductManager();

    public void showMenu() {
        System.out.println("1. findProduct by name");
        System.out.println("2. findProduct by categoryID");
        System.out.println("3. findProduct by price");
        System.out.println("4. sortByPrice");
        System.out.println("5. sortByName");
        System.out.println("6. sortByCategoryName");
        System.out.println("7. mapProductByCategory");
        System.out.println("8. minByPrice");
        System.out.println("9. maxByPrice");
        System.out.println("10. calSalary");
        System.out.println("11. calMonth");
    }

    private void show(List<Product> listProduct) {
        for (Product product : listProduct)
            System.out.println(product);
    }

    private void showMap(Map<Product, Category> productByCategory) {
        for (Map.Entry<Product, Category> productCategoryMap : productByCategory.entrySet()) {
            System.out.println(productCategoryMap.getKey() + ":" + productCategoryMap.getValue().toString());
        }
    }

    public void findByName(List<Product> listProduct) {
        System.out.println("Name of product: ");
        String name = scan.nextLine();
        Product foundProduct = ProductManager.findProduct(listProduct, name);
        System.out.println(foundProduct);
    }

    public void findByCategoryId(List<Product> listProduct) {
        System.out.println("Nhập id: ");
        int categoryId = Integer.parseInt(scan.nextLine());
        List<Product> productsFindById = ProductManager.findProductByCategory(listProduct, categoryId);
        show(productsFindById);
    }

    public void findByPrice(List<Product> listProduct) {
        System.out.println("Nhập giá: ");
        int price = Integer.parseInt(scan.nextLine());
        List<Product> productsFindByPrice = ProductManager.findProductByPrice(listProduct, price);
        show(productsFindByPrice);
    }

    public void sortByPrice(List<Product> listProduct) {
        List<Product> productsSortByPrice = ProductManager.sortByPrice(listProduct);
        show(productsSortByPrice);
    }

    // insert sort
    public void sortByName(List<Product> listProduct) {
        List<Product> productsSortByName = ProductManager.sortByName(listProduct);
        show(productsSortByName);
    }

    public void sortByCategoryName(List<Product> listProduct, List<Category> listCategory) {
        List<Product> productsSortByCategoryName = ProductManager.sortByCategoryName(listProduct, listCategory);
        show(productsSortByCategoryName);
    }

    public void mapProductByCategory(List<Product> listProduct, List<Category> listCategory) {

        List<ProductWithCategoryName> productsWithCategoryName = ProductManager.mapProductByCategory(listProduct, listCategory);
        showProductWithCategoryName(productsWithCategoryName);
//        System.out.println(productsWithCategoryName);
    }

    private void showProductWithCategoryName(List<ProductWithCategoryName> productsWithCategoryName) {
        for (ProductWithCategoryName product : productsWithCategoryName) {
            System.out.println(product);
        }
    }

    public void minByPrice(List<Product> products) {
        Product minProduct = ProductManager.minByPrice(products);
        System.out.println(minProduct);
    }

    public void maxByPrice(List<Product> products) {
        Product maxProduct = ProductManager.maxByPrice(products);
        System.out.println(maxProduct);
    }

    public void calSalary(int salary, int n) {
        int luong = ProductManager.calSalary(salary, n);
        System.out.println(luong);
    }

    public void calMonth(int money, int rate) {
        int month = ProductManager.calMonth(money, rate);
        System.out.println(month);
    }
}
