import java.util.*;

public class ProductManager {
    private static int month ;
    public static Product findProduct(List<Product> listProduct, String nameProduct) {
        for (Product product : listProduct) {
            if (product.getNameProduct().equals(nameProduct)) {
                return product;
            }
        }
        return null;
    }


    public static List<Product> findProductByPrice(List<Product> listProduct, int price) {
        List<Product> listProductByPrice = new ArrayList<Product>();
        for (Product product : listProduct) {
            if (product.getPrice() <= price) {
                listProductByPrice.add(product);
            }
        }
        return listProductByPrice;

    }

    public static List<Product> sortByPrice(List<Product> products) {
        boolean swapped = false;
        for (int i = 0; i < products.size() - 1; i++) {
            for (int j = 0; j < products.size() - 1 - i; j++) {
                if (products.get(j).getPrice() > products.get(j + 1).getPrice()) {
                    Collections.swap(products, j, j + 1);
                    swapped = true;
                }
            }
        }

        return products;
    }

    public static List<Product> sortByName(List<Product> products) {
        int holePosition;
        int i;

        for (i = 1; i < products.size(); i++) {

            // chọn đối tượng cần chèn chèn
            Product tmpProduct = products.get(i);
            // chọn vị trí chèn
            holePosition = i;

            while (holePosition > 0 && products.get(holePosition - 1).getNameProduct().length() < tmpProduct.getNameProduct().length()) {

                products.set(holePosition, products.get(holePosition - 1));
                holePosition--;
            }

            products.set(holePosition, tmpProduct);

        }
        return products;
    }

    public static List<Product> findProductByCategory(List<Product> listProduct, int categoryId) {
        List<Product> listProductByCategory = new ArrayList<Product>();
        for (Product product : listProduct) {
            if (product.getCategoryId() == categoryId) {
                listProductByCategory.add(product);
            }
        }
        return listProductByCategory;

    }

    private static List<Product> listProductByCategoryId(List<Product> listProduct, List<Category> listCategory) {
        List<Product> products = new ArrayList<>();
        for (Category category : listCategory) {
            products.addAll(findProductByCategory(listProduct, category.getId()));
        }
        listProduct = products;
        return listProduct;
    }

    public static List<Product> sortByCategoryName(List<Product> listProduct, List<Category> listCategory) {
        int temp;
        listProduct = listProductByCategoryId(listProduct, listCategory);
        for (int i = 1; i < listProduct.size(); i++) {
            temp = i;
            Product tempProduct = listProduct.get(i);
            while (temp > 0 && listCategory.get(listProduct.get(temp - 1).getCategoryId() - 1).getName().compareTo(listCategory.get(tempProduct.getCategoryId() - 1).getName()) > 0) {
                listProduct.set(temp, listProduct.get(temp - 1));
                temp = temp - 1;
            }
            listProduct.set(temp, tempProduct);
        }
        return listProduct;
    }

    //

    // trả về danh sách product có thêm attribute nameCategory ProductWithCategoryName
    public static List<ProductWithCategoryName> mapProductByCategory(List<Product> listProduct, List<Category> listCategory) {
        List<ProductWithCategoryName> listProductWithCategoryNames = new ArrayList<>();
        for (int i = 0; i < listProduct.size(); i++) {
            listProductWithCategoryNames.add(i, new ProductWithCategoryName(listProduct.get(i).getNameProduct(), listProduct.get(i).getPrice(), listProduct.get(i).getQuality(), listProduct.get(i).getCategoryId(), "sadsad"));
            listProductWithCategoryNames.get(i).setNameCategory(CategoryManager.getNameCategoryById(listCategory, listProduct.get(i).getCategoryId()));
        }
        return listProductWithCategoryNames;
    }


    private static Category findCategoryById(List<Category> listCategory, int categoryId) {
        Category categoryFindById = new Category();
        for (Category category : listCategory) {
            if (category.getId() == categoryId) {
                categoryFindById = category;
            }
        }
        return categoryFindById;
    }

    public static Product minByPrice(List<Product> products) {
        Product minPriceProduct = products.get(0);
        for (Product product : products) {
            if (minPriceProduct.getPrice() > product.getPrice()) {

                minPriceProduct = product;
            }
        }
        return minPriceProduct;
    }

    public static Product maxByPrice(List<Product> products) {
        Product maxPriceProduct = products.get(0);
        for (Product product : products) {

            if (maxPriceProduct.getPrice() < product.getPrice()) {
                maxPriceProduct = product;

            }
        }
        return maxPriceProduct;
    }

    public static int calSalary(int salary, int n) {
        // đệ quy
        if (n <= 1) {
            return salary;
        } else {
            // luong n = luong n -1 + luong n-2
            return calSalary(salary, n - 1) + calSalary(salary / 10, n - 1);
        }

        // không đệ quy

//        for (int i = 2; i <= n; i++) {
//            salary +=  salary *10/100;
//        }
//        return salary ;
    }

//    public static int calMonth(int money, int rate) {
//        int month = 0;
//
//        int tmpMoney = money;
//
//        int doubleMoney = 2 * money;
//
//        while (tmpMoney < doubleMoney) {
//            month++;
//            tmpMoney += tmpMoney * rate / 100;
//
//        }
//        System.out.println(tmpMoney);
//        return month;
//    }



    public static int calMonth(int money, int rate) {
        int tmpMoney = money;
        int tmpMonth = month;
        month++;

        while (tmpMonth > 0) {
            tmpMoney +=  tmpMoney * rate/100;
            tmpMonth--;
        }

        if (tmpMoney < 2 * money) {
            return calMonth(money, rate);
        } else {
            return month - 1;
        }

    }


}
