import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainApplication {
    public static void main(String[] args) {

        // findProduct
        Product product1 = new Product("CPU", 750, 10, 1);
        Product product2 = new Product("RAM", 50, 2, 2);
        Product product3 = new Product("HDD", 70, 1, 2);
        Product product4 = new Product("Main", 400, 3, 1);
        Product product6 = new Product("VGA", 60, 35, 3);
        Product product7 = new Product("VGA", 60, 35, 3);
        Product product5 = new Product("Keyboard", 30, 8, 4);

        List<Product> listProduct = new ArrayList<>();
        listProduct.add(product1);
        listProduct.add(product2);
        listProduct.add(product3);
        listProduct.add(product4);
        listProduct.add(product5);
        listProduct.add(product6);
        listProduct.add(product7);

        Category category1 = new Category(1, "Comuter");
        Category category2 = new Category(2, "Memory");
        Category category3 = new Category(3, "Card");
        Category category4 = new Category(4, "Acsesory");

        List<Category> listCategory = new ArrayList<>();
        listCategory.add(category1);
        listCategory.add(category2);
        listCategory.add(category3);
        listCategory.add(category4);


//       Product foundProduct =  Bai1FindProduct.findProduct(listProduct,"CPU");
        ConsoleManger consoleManger = new ConsoleManger();
        Scanner scanner = new Scanner(System.in);
        consoleManger.showMenu();

        int choice;

        do {
            System.out.print("Mời chọn :");
            choice = Integer.parseInt(scanner.nextLine());
            switch (choice) {
                case 1:
                    consoleManger.findByName(listProduct);
                    break;
                case 2:
                    consoleManger.findByCategoryId(listProduct);
                    break;
                case 3:
                    consoleManger.findByPrice(listProduct);
                    break;
                case 4:
                    consoleManger.sortByPrice(listProduct);
                    break;
                case 5:
                    consoleManger.sortByName(listProduct);
                    break;
                case 6:
                    consoleManger.sortByCategoryName(listProduct, listCategory);
                    break;
                case 7:
                    consoleManger.mapProductByCategory(listProduct, listCategory);
                    break;
                case 8:
                    consoleManger.minByPrice(listProduct);
                    break;
                case 9:
                    consoleManger.maxByPrice(listProduct);
                    break;
                case 10:
                    consoleManger.calSalary(200, 2);
                    break;
                case 11:
//                    int money = Integer.parseInt(scanner.nextLine());
//                    int rate = Integer.parseInt(scanner.nextLine());
                    consoleManger.calMonth(1000, 50);
                    break;
            }
        } while (choice != 4);


    }
}
