public class Product {
    protected String name;
    protected int price;
    protected int quality;
    protected int categoryId;

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", quality=" + quality +
                ", categoryId=" + categoryId +
                '}';
    }

    public Product() {
    }

    public Product(String name, int price, int quality, int categoryId) {
        this.name = name;
        this.price = price;
        this.quality = quality;
        this.categoryId = categoryId;
    }

    public String getNameProduct() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
