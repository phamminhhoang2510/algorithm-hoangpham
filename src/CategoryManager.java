import java.util.List;

public class CategoryManager {

    public static String getNameCategoryById(List<Category> listCategory, int categoryId) {
        String nameCategoryById = "";
        for (Category ct : listCategory) {
            if (ct.getId() == categoryId) {
                nameCategoryById = ct.getName();
            }
        }

        return nameCategoryById;
    }


}
