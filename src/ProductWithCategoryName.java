public class ProductWithCategoryName extends Product {
    private String nameCategory;

    public ProductWithCategoryName(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public ProductWithCategoryName(String name, int price, int quality, int categoryId, String nameCategory) {
        super(name, price, quality, categoryId);
        this.nameCategory = nameCategory;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    @Override
    public String toString() {
        return "ProductWithCategoryName{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", quality=" + quality +
                ", categoryId=" + categoryId +
                ", nameCategory='" + nameCategory + '\'' +
                '}';
    }


}
